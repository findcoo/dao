##스프링 DAO 개요

2. *주의 및 참고*
	+ DAO는 데이터베이스에서 데이터를 읽거나 쓰는 수단을 제공하기 위해 존재한다.
	+ DAO는 서비스가 데이터 베이스에 직접 연결될 필요를 줄여 단위 테스트 효율을 증가시킨다.
	+ 퍼시스턴스 기술과 애플리케이션과의 결합도를 낮춤으로 유연한 설계를 가능케한다.
	+ 데이터 엑세스 절차상 고정된 단계(필수)를 템플릿이라 한다.
	+ 데이터 엑세스 절차상 가변적인 단계(상대적으로 적용)를 콜백이라 한다.
	+ 템플릿은 트랜잭션 제어, 자원관리 및 예외처리 콜백은 질의객체 생성, 파라미터 바인딩, 질의 결과 추출과 변환등로 이루어진다.
	+ 템플릿은 퍼시스턴스 프레임워크 별로 지원되며 JdpcTemplate를 사용하거나 ORM형식을 사용할 경우 JPA, 하이버네이트를 사용한다.
	+ 템플릿 객체를 컨텍스트 빈으로 구성하고 DAO와 와이어링함으로 템플릿을 사용한다.
	+ DAO 지원 클래스르 사용하여 템플릿 사용을 단순화 할 수 있다.
3. *퍼시스턴스 메커니즘에 따른 데이터 엑세스 템플릿*

| 템플릿 클래스 | 용도 |
|:-----------|:----|
| jca.cci.core.CciTemplate | JCA CCI 연결 |
| jdbc.core.JdbcTemplate | JDBC 연결 |
| jdbc.core.namedparam.NamedParameterJdbcTemplate | 명명된 파라미터가 지원되는 JDBC 연결 |
| jdbc.core.simple.SimpleJdbcTemplate | 자바 5를 활용해서 더 쉬워진 JDBC 연결 |
| orm.hibernate.HibernateTemplate | Hibernate 2.x 세션 |
| orm.hibernate.HibernateTemplate | Hibernate 3.x 세션 |
| orm.ibatis.SqlMapClientTemplate | IBATIS SqlMap 클라이언트 |
| orm.jdo.JdoTemplate | JDO 구현체 |
| orm.jpa.JpaTemplate | JPA 엔티티 관리자 |
4. *DAO 지원 클래스에 대응하는 템플릿*

| DAO 지원 클래스 | 용도 |
|:-------------|:-----|
| jca.cci.supprot.CciDaoSupport | JCA CCI 연결 |
| jdbc.core.support.JdbcDaoSupport | JDBC 연결 |
| jdbc.core.namedparam.NamedParameterJdbcDaoSupport | 명명된 파리미터가 지원되는 JDBC 연결 |
| jdbc.core.simple.SimpleJdbcDaoSupport | 자바 5를 활용해서 더 쉬워진 JDBC 연결 |
| orm.hibernate.support.HibernateDaoSupport | Hibernate 2.x 세션 |
| orm.hibernate3.support.HibernateDaoSupprot | Hibernate 3.x 세션 |
| orm.ibatis.support.SqlMapClientDaoSupport | iBATIS SqlMap 클라이언트 |
| orm.jdo.support.JdoDaoSupport | JDO 구현체 |
| orm.jpa.support.JpaDaoSupport | JPA 엔티티 관리자 |
 
